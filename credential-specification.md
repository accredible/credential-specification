# Credential

Credentials are representations of knowledge or a skill and may demonstrate the completion of an experience in which knowledge and skills are gained.

A credential is a file containing a well-formatted credential representation in JSON served with the content-type `application/json`. This lives at a stable URL (for example, [https://www.accredible.com/1234.json](https://www.accredible.com/1234.json)). Stakeholders make requests to this address to view and verify credentials.


## Credential Specification

### Structures

Fields marked **in bold letters** are mandatory.


#### Credential

| Property | Expected Type | Description |
| -------- | ------------- | ----------- |
| **id** | Integer | Unique Identifier for the badge. This is expected to be **globally** unique. |
| **recipient** | [Individual](#individual) | The recipient of the achievement. |
| **issuer** | [Organization](#organization) | The issuing organization. |
| **name** | Text | The name of the achievement. |
| description | Text | A short description of the achievement. |
| **issued_on** | [DateTime](#datetime) | Date that the achievement was awarded. |
| evidence_items | Array of [EvidenceItem](#evidenceitem)s | The artefacts that demonstrate or provide credibility toward the acheivment. |
| references | Array of [Reference](#reference)s | Endorsements from stakeholders which add credibility to the acheivment. |


#### <a id="individual"></a>Individual

Property | Expected Type | Description
--------|------------|-----------
**name** | Text | The name of the person.
**email_hash** | EmailHash | The hashed email address of the person.
url | URL | Personal online identity (LinkedIn, About.me, etc).


#### <a id="organization"></a>Organization

Property | Expected Type | Description
--------|------------|-----------
**name** | Text | The name of the issuing organization.
**url** | URL | URL of the institution
description | Text | A short description of the institution
image | [Data URL](http://en.wikipedia.org/wiki/Data_URI_scheme) or URL | An image representing the institution


#### <a id="evidenceitem"></a>EvidenceItem

Property | Expected Type | Description
--------|------------|-----------
**description** | Text | Description of the artefact.
url | URL | URL linking to an artefact.
file | URL | The artefact file.

#### <a id="reference"></a>Reference

Property | Expected Type | Description
--------|------------|-----------
**description** | Text | Description of the endorsement.
**referee** | Individual | The person providing the endorsement.
**relationship** | Text | Description of the relationship between the referee and the individual receiving the endorsement.


### <a id="additional-properties"></a>Additional Properties

Additional properties are allowed so long as they don't clash with specified
properties. **Processors should preserve all properties when
retransmitting**.


### Primitives

* Boolean
* Integer
* Text
* Array
* <a id="date-time"></a>DateTime - Either an [ISO 8601](http://en.wikipedia.org/wiki/ISO_8601) date or a standard 10-digit Unix timestamp.
* URL - Fully qualified URL, including protocol, host, port if applicable, and path.
* <a id="emailhash"></a>EmailHash - A hash string preceded by a dollar sign ("$") and the algorithm used to generate the hash. For example: `sha256$28d50415252ab6c689a54413da15b083034b66e5` represents the result of calculating a SHA256 on the string "mayze". For more information, see [how to hash & salt in various languages](https://github.com/mozilla/openbadges/wiki/How-to-hash-&-salt-in-various-languages.).

## JSON Examples

A valid credential:

* _Credential:_ contains information regarding a specific credential assigned to a user
https://www.accredible.com/1234.json
```json
    {
        credential: {
            recipient: {
                "name": "John Doe",
                "email_hash": "sha256$c7ef86405ba71b85acd8e2e95166c4b111448089f2e1599f42fe1bba46e865c5"
            },
            issuer: {
                "name": "Course Provider",
                "url": "http://www.awesomelearningexample.com",
                "description": "Technical courses at an introductory level..."
                "image": "http://www.awesomelearningexample.com/logo.png"
            },
            name: "Credential Name",
            description: "A detailed description of what the credential is for",
            issued_on: 1359217910,
            evidence_items: [
                { 
                    "description": "Report card including all grades",
                    "url": "http://www.awesomelearningexample.com/johndoe/reportcard"
                },
                { 
                    "description": "Final essay",
                    "file": "https://www.accredible.com/1234/essay.doc"
                }
            ],
            references: [
                {
                    "description": "John worked hard on this course and provided exemplary understanding of the core concepts",
                    "referee": {
                        "name": "Jane Doe",
                        "email_hash": "sha256$c7ef86405ba71b85acd8e2e95166c4b111448089f2e1599f42fe1bba46e865c5"
                    },
                    "relationship": "managed"
                }
            ]
        }
    }
```